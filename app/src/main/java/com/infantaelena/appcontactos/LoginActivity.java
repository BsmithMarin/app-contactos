package com.infantaelena.appcontactos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_AppContactos); //Cambia al tema principal de la App
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    }
}