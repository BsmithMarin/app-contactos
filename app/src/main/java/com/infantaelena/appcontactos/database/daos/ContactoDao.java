package com.infantaelena.appcontactos.database.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.infantaelena.appcontactos.database.modelos.Contacto;

import java.util.List;

@Dao
public interface ContactoDao {

    @Insert
    long insertContacto (Contacto contacto);

    @Update
    int updateContacto(Contacto contacto);

    @Delete
    int deleteContacto (Contacto contacto);

    @Query("SELECT * FROM contacto WHERE id_usuario=:id_usuario")
    List<Contacto> getAllContactsOfUser(long id_usuario);

}
