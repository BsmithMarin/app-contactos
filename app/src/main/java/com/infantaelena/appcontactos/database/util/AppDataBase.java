package com.infantaelena.appcontactos.database.util;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.infantaelena.appcontactos.database.daos.ContactoDao;
import com.infantaelena.appcontactos.database.daos.UsuarioDao;
import com.infantaelena.appcontactos.database.modelos.Contacto;
import com.infantaelena.appcontactos.database.modelos.Usuario;


/**
 * @author Brahyan Marin
 * Clase abstracta que permite interactuar con la BD de la aplicación
 */

@Database(entities = {Usuario.class, Contacto.class},version = 1)
public abstract class AppDataBase extends RoomDatabase {

    public abstract UsuarioDao usuarioDao();

    public abstract ContactoDao contactoDao();

}
