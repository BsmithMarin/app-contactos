package com.infantaelena.appcontactos.database.modelos;


import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;


/**
 * @author Brahyan Marin
 * Crea relación entre las entidades Usuario y Contacto de la BD
 */

public class UsuarioConContactos {

    @Embedded public Usuario usuario;

    @Relation(
            parentColumn = "id",
            entityColumn = "id_usuario"
    )

    public List<Contacto> contactos;

}
