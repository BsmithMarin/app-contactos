package com.infantaelena.appcontactos.database.listeners;

import com.infantaelena.appcontactos.database.modelos.Contacto;

import java.util.EventListener;

/**
 * @author Brahyan Marin
 * interfaz que define el callback al insertarse un nuevo contacto en la BD
 */

@FunctionalInterface
public interface ContactoListener extends EventListener {

    void contactoInsertado(Contacto contacto);

}
