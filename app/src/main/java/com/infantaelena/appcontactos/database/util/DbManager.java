package com.infantaelena.appcontactos.database.util;

import android.content.Context;
import android.util.Log;

import androidx.room.Room;


import com.infantaelena.appcontactos.database.modelos.Contacto;
import com.infantaelena.appcontactos.database.modelos.Usuario;
import com.infantaelena.appcontactos.database.modelos.UsuarioConContactos;
import com.infantaelena.appcontactos.database.listeners.ContactoListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brahyan Marin
 * Gestiona la manipulación de la base de datos usando patrón singleton
 */

public class DbManager {

    private final AppDataBase appDataBase;

    private static DbManager dbManager;

    private final List<ContactoListener> listenersContacto;


    private DbManager(Context context) {
        appDataBase = Room.databaseBuilder(context,
                AppDataBase.class,
                "appdb").build();

        listenersContacto = new ArrayList<>();
    }

    /**
     * Devuelve DbManger con el cotexto de la aplicación, se usa patrón singleton, ya que el contexto
     * de la aplicación es siempre el mismo y la creacion de instancias de appDataBase es constosa
     *
     * @param context
     * @return
     */
    public synchronized static DbManager getInstance(Context context) {

        if (dbManager == null) {
            dbManager = new DbManager(context.getApplicationContext());
        }
        return dbManager;
    }

    /**
     * Inserta usuario en la base datos
     *
     * @param usuario
     * @return true si el usuario se inserto correctamente, false si viola alguna restricción
     */
    public synchronized boolean insertUsuario(Usuario usuario) {

        boolean insertado = false;

        try {
            usuario.id = appDataBase.usuarioDao().insertarUsuario(usuario);
            insertado = true;
        } catch (Exception e) {
            Log.d("BD", "restriccion no respetada", e);
        }

        return insertado;
    }

    /**
     * Inserta contaco en la BD e informa a los listeners de la existencia de un nuevo contacto.
     *
     * @param contacto
     * @return
     */
    public synchronized boolean insertContacto(Contacto contacto) {

        boolean insertado = false;

        try {
            contacto.id_contacto = appDataBase.contactoDao().insertContacto(contacto);
            insertado = true;
            notifyContactoModified(contacto);
        } catch (Exception e) {
            Log.d("BD", "CONTACTO YA EXISTENTE", e);
        }

        return insertado;
    }

    /**
     * Devuelve usuario por nombre de usuario, que es unico para cada usuario.
     *
     * @param username
     * @return
     */
    public synchronized Usuario getUsurioByUsername(String username) {

        return appDataBase.usuarioDao().getUsuarioByUsername(username);
    }

    /**
     * Devuelve usuario con una lista de sus contactos a partir de objeto usuario.
     *
     * @param usuario
     * @return
     */
    public synchronized UsuarioConContactos getUserWithContacts(Usuario usuario) {
        return appDataBase.usuarioDao().getUsuarioWithContacts(usuario.id);
    }

    /**
     * Borra el contacto de la base de datos, si se produce borrado se informa a los listeners
     *
     * @param contacto
     */
    public synchronized void deleteContacto(Contacto contacto) {

        appDataBase.contactoDao().deleteContacto(contacto);
    }

    public synchronized boolean updateContacto(Contacto contacto) {

        boolean modificado = false;

        try {
            int rowsModified = appDataBase.contactoDao().updateContacto(contacto);

            if (rowsModified > 0)
                modificado = true;

        } catch (Exception e) {
            Log.d("BD", "Las modificaciones infringen las restriciones", e);
        }
        return modificado;
    }


    //Getion de listeners

    public synchronized void addContactoListener(ContactoListener listener) {
        listenersContacto.add(listener);
    }

    public synchronized void removeContactoListener(ContactoListener listener) {
        listenersContacto.remove(listener);
    }

    public synchronized void notifyContactoModified(Contacto contacto) {
        for (ContactoListener listener : listenersContacto) {
            listener.contactoInsertado(contacto);
        }
    }
}
