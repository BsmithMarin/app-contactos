package com.infantaelena.appcontactos.database.daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import com.infantaelena.appcontactos.database.modelos.Usuario;
import com.infantaelena.appcontactos.database.modelos.UsuarioConContactos;


@Dao
public interface UsuarioDao {

    @Insert
    long insertarUsuario(Usuario usuario);

    @Query("SELECT * FROM usuario WHERE username=:username")
    Usuario getUsuarioByUsername(String username);

    @Transaction
    @Query("SELECT * FROM usuario WHERE id=:id")
    UsuarioConContactos getUsuarioWithContacts(long id);

}
