package com.infantaelena.appcontactos.database.modelos;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;

/**
 * @author Brahyan Marin
 * Modelo de los contactos de los diferentes usuarios de la App
 */

@Entity(tableName = "contacto",
        foreignKeys = {@ForeignKey(entity = Usuario.class,
                parentColumns = "id",
                childColumns = "id_usuario",
                onDelete = ForeignKey.CASCADE)
        }, indices = {@Index(value = "nombre", unique = true)})
public class Contacto implements Serializable, Comparable<Contacto> {

    @PrimaryKey(autoGenerate = true)
    public long id_contacto;

    @ColumnInfo(name = "id_usuario")
    public long id_usuario;

    @ColumnInfo(name = "nombre")
    public String nombre;

    @ColumnInfo(name = "email")
    public String email;

    @ColumnInfo(name = "num_telf")
    public String numeroTelf;

    public Contacto(String nombre, String email, String numeroTelf, long id_usuario) {
        this.nombre = nombre;
        this.email = email;
        this.numeroTelf = numeroTelf;
        this.id_usuario = id_usuario;
    }

    public Contacto() {
    }

    @Override
    public String toString() {
        return "Contacto{" +
                "id_contacto=" + id_contacto +
                ", id_usuario=" + id_usuario +
                ", nombre='" + nombre + '\'' +
                ", email='" + email + '\'' +
                ", numeroTelf='" + numeroTelf + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contacto contacto = (Contacto) o;
        return id_contacto == contacto.id_contacto && id_usuario == contacto.id_usuario && Objects.equals(nombre, contacto.nombre) && Objects.equals(email, contacto.email) && Objects.equals(numeroTelf, contacto.numeroTelf);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_contacto, id_usuario, nombre, email, numeroTelf);
    }

    @Override
    public int compareTo(Contacto contacto) {

        return this.nombre.toLowerCase(Locale.ROOT).compareTo(
                contacto.nombre.toLowerCase(Locale.ROOT));
    }
}
