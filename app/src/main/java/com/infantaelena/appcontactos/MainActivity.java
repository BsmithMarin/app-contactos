package com.infantaelena.appcontactos;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.infantaelena.appcontactos.database.modelos.Usuario;
import com.infantaelena.appcontactos.fragments.main.FragmentContactList;

public class MainActivity extends AppCompatActivity {

    Usuario usuario;
    Toolbar mainToolbar;

    /**
     * Carga la vista y muestra el fragmento de la lista de contactos
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_AppContactos);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainToolbar = findViewById(R.id.tbMain);
        setSupportActionBar(mainToolbar);

        usuario = (Usuario) (getIntent().getSerializableExtra("user"));

        FragmentContactList fragment = new FragmentContactList(usuario);

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.slide_in,
                        R.anim.fade_out,
                        R.anim.fade_in,
                        R.anim.slide_out
                )
                .setReorderingAllowed(true).
                replace(R.id.mainFragmentContainer, fragment, null).
                commit();
    }

    /**
     * Crea menu superior para toda la actividad
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    /**
     * Borra las credenciales de usuaro guardadas en SharedPreferences si las hubiese, y cierra la
     * aplicacion, este metodo se llama al pulsar en la opcion sing out de la barra de herramientas.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.singOut) {

            SharedPreferences preferences = getSharedPreferences("credentials", Context.MODE_PRIVATE);
            String user = preferences.getString("username", null);
            Log.d("LOL", "user" + user);
            if (user != null) {
                Log.d("LOL", "MODIFICAON");
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("username", null);
                editor.putString("password", null);
                editor.apply();
            }
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}