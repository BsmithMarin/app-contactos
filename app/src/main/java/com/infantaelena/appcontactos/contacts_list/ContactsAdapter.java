package com.infantaelena.appcontactos.contacts_list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.infantaelena.appcontactos.R;
import com.infantaelena.appcontactos.database.modelos.Contacto;
import com.infantaelena.appcontactos.fragments.main.FragmentContactDetails;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactViewHolder> {

    List<Contacto> listaContactos;

    public ContactsAdapter(List<Contacto> contactos){
        listaContactos = contactos;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_contact,parent,false);

        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {

        Contacto contacto = listaContactos.get(position);

        holder.bind(contacto);

        holder.itemView.setOnClickListener((view) -> {

            FragmentContactDetails fragment = new FragmentContactDetails(contacto);

            ((AppCompatActivity)view.getContext()).getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                    R.anim.slide_in,
                    R.anim.fade_out,
                    R.anim.fade_in,
                    R.anim.slide_out)
                    .replace(R.id.mainFragmentContainer,fragment)
                    .addToBackStack(null)
                    .commit();
        });
    }

    @Override
    public int getItemCount() {
        return listaContactos.size();
    }
}
