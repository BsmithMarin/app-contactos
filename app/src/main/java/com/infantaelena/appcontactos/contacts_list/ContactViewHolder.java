package com.infantaelena.appcontactos.contacts_list;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.infantaelena.appcontactos.R;
import com.infantaelena.appcontactos.database.modelos.Contacto;

public class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView ivMail, ivPhone;
    TextView tvContactName;

    Contacto contacto;

    Resources resources;

    public ContactViewHolder(@NonNull View itemView) {
        super(itemView);

        ivMail = itemView.findViewById(R.id.ivMail);
        ivPhone = itemView.findViewById(R.id.ivPhone);
        tvContactName = itemView.findViewById(R.id.tvContactName);

        resources = tvContactName.getResources();
    }

    /**
     * Une los datos del contacto con la vista de carta que lo representa en la lista de
     * contactos
     * @param contacto
     */
    public void bind(Contacto contacto) {
        this.contacto = contacto;
        tvContactName.setText(contacto.nombre);
        ivMail.setOnClickListener(this);
        ivPhone.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == ivMail) {

            enviarCorreo();

        } else if (view == ivPhone) {

            llamar();
        }
    }

    /**
     * Solicita al usuario que seleccione una aplicación para enviar un correo al correo del
     * contacto
     */
    private void enviarCorreo() {

        Intent correo = new Intent(Intent.ACTION_SEND);
        correo.setType("*/*");
        correo.putExtra(Intent.EXTRA_EMAIL, new String[]{contacto.email});
        ivMail.getContext().startActivity(correo);
    }

    /**
     * Si se tienen permisos para acceder al telefono se llama al usuario, si no se tienen permisos
     * se solicitan.
     */
    private void llamar() {

        if (ContextCompat.checkSelfPermission(ivPhone.getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            //Solicitamos permiso de telefono
            requestPhonePermission();
        } else {
            Intent llamanda = new Intent(Intent.ACTION_CALL);
            llamanda.setData(Uri.parse("tel:" + contacto.numeroTelf));
            ivPhone.getContext().startActivity(llamanda);
        }
    }

    /**
     * Solicita permisos para usar el teléfono, si ya han sido rechazados con anterioridad muestra
     * mensaje Toast
     */
    private void requestPhonePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) ivPhone.getContext(), Manifest.permission.CALL_PHONE)) {
            //El usuario ya ha rechazado los permisos con anterioridad
            Toast.makeText(ivPhone.getContext(),
                    resources.getString(R.string.phone_permission_dinied),
                    Toast.LENGTH_SHORT).show();
        } else {
            //pide permisos de telefono
            ActivityCompat.requestPermissions((Activity) ivPhone.getContext(), new String[]{Manifest.permission.CALL_PHONE}, 255);
            llamar();
        }

    }


}
