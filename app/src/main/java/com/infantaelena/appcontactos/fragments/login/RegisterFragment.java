package com.infantaelena.appcontactos.fragments.login;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.infantaelena.appcontactos.R;
import com.infantaelena.appcontactos.database.modelos.Usuario;
import com.infantaelena.appcontactos.database.util.DbManager;


/**
 * @author Brahyan Marin
 * Valida los datos del nuevo usuarioi y lo inserta en la BD
 */
public class RegisterFragment extends Fragment implements View.OnClickListener{


    EditText etUsernameRegister,etPassRegister,etPassRepeatRegister ;
    Button btnSingUp;

    String username,pass,passRepeat;

    DbManager db;

    Resources resources;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Thread(()->{
            db = DbManager.getInstance(getContext());
        }).start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etUsernameRegister = view.findViewById(R.id.etUsernameRegister);
        etPassRegister = view.findViewById(R.id.etPassRegister);
        etPassRepeatRegister = view.findViewById(R.id.etPassRepeatRegister);
        btnSingUp = view.findViewById(R.id.btnSingUp);

        btnSingUp.setOnClickListener(this);

        resources = getResources();
    }


    @Override
    public void onClick(View view) {

        if(view ==  btnSingUp){

            username = etUsernameRegister.getText().toString().trim();
            pass = etPassRegister.getText().toString();
            passRepeat = etPassRepeatRegister.getText().toString();
            //Si los campos son validos intenta crear nuevo usuario
            if(nameAndPassValidos())
                new Thread(this::registrarUsuario).start();
        }
    }

    /**
     * Registra nuevo usuario en la base de datos a partir de los datos introducidos
     */
    private void registrarUsuario(){

        boolean insertado = db.insertUsuario(new Usuario(username,pass));

        if (!insertado){
            getActivity().runOnUiThread(()->{
                mensajeToast(resources.getString(R.string.unavailable_username));
            });
        }else {
            getActivity().runOnUiThread(()->{
                mensajeToast(resources.getString(R.string.user_created));
            });
           getActivity()    //Vuelve al fragmento de login
                   .getSupportFragmentManager()
                   .popBackStack();
        }
    }

    /**
     * Comprueba que el nombre de usuario tiene al menos 4 caracteres, y que las contraseñas
     * sean iguales y que tengan al menos 4 caracteres.
     */
    private boolean nameAndPassValidos(){

        boolean validos = true;

        if (username.trim().length() < 4){
            mensajeToast(resources.getString(R.string.invalid_username));
            validos = false;
        }

        if( pass.length() < 4 ){
            mensajeToast(resources.getString(R.string.invalid_pass));
            validos = false;
        }

        if( !pass.equals(passRepeat) ){
            mensajeToast(resources.getString(R.string.pass_mismatch));
            validos = false;
        }

        return validos;
    }

    /**
     * Muestra mensaje toast
     * @param mensaje
     */
    private void mensajeToast(String mensaje){
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}