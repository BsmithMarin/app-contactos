package com.infantaelena.appcontactos.fragments.main;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.infantaelena.appcontactos.R;
import com.infantaelena.appcontactos.database.modelos.Contacto;
import com.infantaelena.appcontactos.database.util.DbManager;


public class FragmentContactDetails extends Fragment implements View.OnClickListener {

    EditText etContactNameEdit, etContactEmailEdit, etContactPhoneEdit;
    FloatingActionButton fabSaveChanges;

    Contacto contacto;

    DbManager db;

    public FragmentContactDetails() {
    }

    public FragmentContactDetails(Contacto contacto) {
        this.contacto = contacto;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        new Thread(() -> {
            db = DbManager.getInstance(getContext());
        }).start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) //Recupera instancias precvias
            contacto = (Contacto) savedInstanceState.getSerializable("contact");

        etContactNameEdit = view.findViewById(R.id.etContactNameEdit);
        etContactEmailEdit = view.findViewById(R.id.etContactEmailEdit);
        etContactPhoneEdit = view.findViewById(R.id.etContactPhoneEdit);
        fabSaveChanges = view.findViewById(R.id.fabSaveChanges);


        //Rellena los campos de texto con la informacion del contacto
        etContactNameEdit.setText(contacto.nombre);
        etContactEmailEdit.setText(contacto.email);
        etContactPhoneEdit.setText(contacto.numeroTelf);

        fabSaveChanges.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == fabSaveChanges) {
            String name = etContactNameEdit.getText().toString();

            if (name.equals("")) {
                mensajeToast(R.string.must_fill_name);

            } else {
                dialogoActualizar();
            }
        }
    }

    /**
     * Muestra cuadro de dialogo antes de aplicar los cambios al contacto
     */
    private void dialogoActualizar() {

        String name = etContactNameEdit.getText().toString();
        String phone = etContactPhoneEdit.getText().toString();
        String mail = etContactEmailEdit.getText().toString();

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle(R.string.modify_contact);
        builder.setMessage(R.string.contact_modify_info);

        builder.setPositiveButton(R.string.accept, (dialog, which) -> {

            Contacto modificar = new Contacto(name, mail, phone, contacto.id_usuario);
            modificar.id_contacto = contacto.id_contacto;

            new Thread(() -> {
                boolean modificado = db.updateContacto(modificar);
                Log.d("LOL", modificar.toString());
                if (modificado) {
                    contacto = modificar;
                    getActivity().runOnUiThread(() ->
                            mensajeToast(R.string.contact_modified)
                    );
                } else {
                    getActivity().runOnUiThread(() ->
                            mensajeToast(R.string.name_already_used)
                    );
                }
            }).start();
        });
        //No hace nada se se cancela
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
        });

        builder.create().show();
    }

    /**
     * Guarda la información del contacto al recrear la vista
     *
     * @param outState
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putSerializable("contact", contacto);
        super.onSaveInstanceState(outState);
    }

    /**
     * Muestra mensaje Toast
     *
     * @param resId
     */
    private void mensajeToast(int resId) {
        Toast.makeText(getContext(),
                resId,
                Toast.LENGTH_SHORT).show();
    }

    /**
     * Añade el icono de borrar a la barra de herramientas
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contact_details, menu);
    }

    /**
     * Borra el contacto si se pulsa el el boton con el icono de la papelera
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.delete_contact) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

            builder.setTitle(R.string.delete_contact_dialog_title);
            builder.setMessage(R.string.delete_contact_dialog_message);

            builder.setPositiveButton(R.string.accept, (dialog, which) -> {
                new Thread(() -> {
                    db.deleteContacto(contacto);
                    getActivity().runOnUiThread(() ->
                            getActivity().onBackPressed()
                    );
                }).start();
            });

            builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
            });

            builder.create().show();
        }

        return super.onOptionsItemSelected(item);
    }

}