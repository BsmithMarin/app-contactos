package com.infantaelena.appcontactos.fragments.main;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.infantaelena.appcontactos.R;
import com.infantaelena.appcontactos.contacts_list.ContactsAdapter;
import com.infantaelena.appcontactos.database.listeners.ContactoListener;
import com.infantaelena.appcontactos.database.modelos.Contacto;
import com.infantaelena.appcontactos.database.modelos.Usuario;
import com.infantaelena.appcontactos.database.util.DbManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Brahyan Marin
 * Muestra dinamicamente la lista de contactos del usuario
 */
public class FragmentContactList extends Fragment implements View.OnClickListener, ContactoListener {

    Usuario usuario;

    RecyclerView rvContactos;
    FloatingActionButton fabAddContact;
    ContactsAdapter contactsAdapter;
    List<Contacto> listaContactos;

    DbManager db;

    public FragmentContactList() {}

    public FragmentContactList(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Thread(() -> {
            db = DbManager.getInstance(getContext());
            db.addContactoListener(this);//Suscripcion a las notificaciones
        }).start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Recupera instancia de usuario en caso de reconstruir la vista
        if (savedInstanceState != null)
            usuario = (Usuario) savedInstanceState.getSerializable("user");


        fabAddContact = view.findViewById(R.id.fabAddContact);
        rvContactos = view.findViewById(R.id.rvContactos);
        fabAddContact.setOnClickListener(this);
        initContactList();
    }

    /**
     * Carga los contactos existentes en la base de datos y la ordena alfabeticamente
     */
    private void initContactList() {
        new Thread(() -> {
            listaContactos = new ArrayList<>();
            listaContactos = db.getUserWithContacts(usuario).contactos;
            Collections.sort(listaContactos);
            contactsAdapter = new ContactsAdapter(listaContactos);
            getActivity().runOnUiThread(() -> {
                rvContactos.setLayoutManager(new LinearLayoutManager(getContext()));
                rvContactos.setAdapter(contactsAdapter);
            });
        }).start();

    }

    /**
     * Guarda el usuario que se necesita para recuperar los contactos de la BD en caso de la vista
     * sea destruida
     *
     * @param outState
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putSerializable("user", usuario);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {

        if (view == fabAddContact) {

            addContactDialog();
        }
    }

    /**
     * Crea cuadro de dialogo para insertar un nuevo contacto
     */
    private void addContactDialog(){
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_contact, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle(R.string.add_contact);
        builder.setView(dialogView);

        builder.setPositiveButton(R.string.accept, (dialog, which) -> {

            String nombre = ((EditText) dialogView.findViewById(R.id.etNombre)).
                    getText().toString();

            String email = ((EditText) dialogView.findViewById(R.id.etEmail)).
                    getText().toString();

            String telefono = ((EditText) dialogView.findViewById(R.id.etTelefono)).
                    getText().toString();

            new Thread(() -> {
                boolean insertado = db.insertContacto(new Contacto(
                        nombre, email, telefono, usuario.id
                ));

                if (!insertado)
                    getActivity().runOnUiThread(() -> {
                        Toast.makeText(getContext(), R.string.name_already_used, Toast.LENGTH_SHORT).show();
                    });
            }).start();
        });

        builder.setNegativeButton(R.string.cancel,(d,w)->{});
        builder.create().show();
    }

    /**
     * Añade a lista el contacto recien insertado a la BD y ordena alfabeticamente la lista
     * @param contacto
     */
    @Override
    public void contactoInsertado(Contacto contacto) {

        getActivity().runOnUiThread(()->{
            
            if(!listaContactos.contains(contacto)){
                listaContactos.add(contacto);
                Collections.sort(listaContactos);
                contactsAdapter.notifyItemInserted(listaContactos.indexOf(contacto));
            }
        });
    }

    /**
     * Elimina suscripcion a la notificiaciones de nuevo contacto
     */
    @Override
    public void onDestroyView() {
        db.removeContactoListener(this);
        super.onDestroyView();
    }
}