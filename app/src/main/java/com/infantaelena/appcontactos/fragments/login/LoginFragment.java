package com.infantaelena.appcontactos.fragments.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.infantaelena.appcontactos.MainActivity;
import com.infantaelena.appcontactos.R;
import com.infantaelena.appcontactos.database.modelos.Contacto;
import com.infantaelena.appcontactos.database.modelos.Usuario;
import com.infantaelena.appcontactos.database.util.DbManager;

/**
 * @author Brahyan Marin
 * Gestión de la vista de la pantalla de login
 */
public class LoginFragment extends Fragment implements View.OnClickListener {


    EditText etUserName, etPassword;
    Button btnLogin;
    CheckBox cbRemember;
    TextView tvRegister;

    DbManager db;

    Resources resources;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Inicia la base de datos
        new Thread(() -> {
            db = DbManager.getInstance(getContext());
        }).start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);//Inicia vistas

        SharedPreferences prefs = getActivity().getSharedPreferences("credentials", Context.MODE_PRIVATE);

        //Si ya existen credenciales guardadas se logea automaticamente
        String username = prefs.getString("username", null);
        String password = prefs.getString("password", null);

        if (username != null && password != null) {
            login(username, password);
        }

    }

    private void init(View view) {
        etUserName = view.findViewById(R.id.etUserName);
        etPassword = view.findViewById(R.id.etPassword);
        btnLogin = view.findViewById(R.id.btnLogin);
        cbRemember = view.findViewById(R.id.cbRemember);
        tvRegister = view.findViewById(R.id.tvRegister);

        tvRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        resources = getResources();
    }

    @Override
    public void onClick(View view) {

        if (view == tvRegister) {

            cambiarFragmentoRegistro();

        } else if (view == btnLogin) {
            String username = etUserName.getText().toString();
            String password = etPassword.getText().toString();
            login(username, password);
        }

    }

    /**
     * Inicia sesion en la aplicacion con las credenciales dadas
     *
     * @param username
     * @param password
     */
    private void login(String username, String password) {

        new Thread(() -> {
            Usuario usuario = db.getUsurioByUsername(username);

            if (usuario == null || !password.equals(usuario.password)) {
                getActivity().runOnUiThread(this::toastInvalidCredentials);
                return;
            }

            if (cbRemember.isChecked())
                guardarCredenciales(username, password);

            getActivity().runOnUiThread(() -> {
                Intent intent = new Intent(getContext(), MainActivity.class);
                intent.putExtra("user", usuario);
                startActivity(intent);
                getActivity().finish();
            });
        }).start();
    }

    /**
     * Cambia al fragmento de registro
     */
    private void cambiarFragmentoRegistro() {

        getActivity().getSupportFragmentManager().
                beginTransaction().
                setCustomAnimations(
                        R.anim.slide_in,
                        R.anim.fade_out,
                        R.anim.fade_in,
                        R.anim.slide_out
                ).
                setReorderingAllowed(true).
                replace(R.id.loginFragmentContainer, RegisterFragment.class, null).
                addToBackStack(null).
                commit();
    }

    private void toastInvalidCredentials() {
        Toast.makeText(getContext(),
                resources.getString(R.string.invalid_login),
                Toast.LENGTH_SHORT).show();
    }

    /**
     * Guarda las credenciales de usuario para futuras ocasiones en la memoria del telefono,
     * usando pares clave-.
     *
     * @param username
     * @param password
     */
    private void guardarCredenciales(String username, String password) {
        SharedPreferences preferences = getActivity().getSharedPreferences("credentials", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username", username);
        editor.putString("password", password);
        editor.apply();
    }
}